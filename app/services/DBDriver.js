import {Http,HTTP_BINDINGS} from 'http/http';
import {Injector,Inject} from 'angular2/di'
export class DBDriver { 
  
  
   constructor(/*@Inject(Http) http:Http*/) {
     console.log('in db ctor');
   
    this.entities = [];
    var injector = Injector.resolveAndCreate([HTTP_BINDINGS]);
    this.http = injector.get(Http);
   
  }
  
  get() {
    return this.entities;
  }
  
  add(value: Entity) {
    this.entities.push(value);
  }
  
   getEntities(cb) {
     console.log('db getEntities');
    
    this.http.get('./entities.1.json')
      //Get the RxJS Subject
      .toRx()
      // Call map on the response observable to get the parsed  object
      .map(function(res) {
          return  res.json()}
          )
      // Subscribe to the observable to get the parsed object and send it to the callback
      .subscribe(cb);

  }
}

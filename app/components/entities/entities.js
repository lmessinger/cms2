import {Component, View, NgFor, NgIf} from 'angular2/angular2';
import {C2Dropdown} from '../C2Dropdown/C2Dropdown';
import {C2DropdownMenu} from '../C2Dropdownmenu/C2DropdownMenu';
import {C2DropdownItem} from '../C2Dropdown/C2DropdownItem/C2DropdownItem';
import {DBDriver} from '../../services/DBDriver';

@Component({
  selector: 'entities',
  bindings: [DBDriver],
  properties: ['selectedValue']
})
@View({
	templateUrl: './components/entities/entities.html',
  directives : [NgFor,C2Dropdown,C2DropdownMenu,C2DropdownItem]
})
export class Entities {
 
  constructor(dbDriver:DBDriver) {
    this.mishtane = 1;
    var that = this;
    that.etts = [];
    dbDriver.getEntities(function(etts) {
      that.etts =  etts;
      });
      
    this.selectedValue = "none";
    
  }
  
  showAll(x) {
    console.log('showAll',x); 
    var eets = this.etts;
    // setTimeout(function() {
    //   console.log('show Etts',x.items,eets); 
    // },1000);
  }
  
  menutoggled($event) {
    console.log('menutoggled event reached entities',$event);
    this.hidden = !this.hidden;
  }
  
  addEntity(selectedTitle) {
    alert(selectedTitle + ' added ');
  }
}

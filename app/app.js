import {Component, View, bootstrap} from 'angular2/angular2';
import {routerInjectables,RouteConfig, RouterOutlet, RouterLink} from 'angular2/router';
import {Http,HTTP_BINDINGS} from 'http/http';

import {Home} from './components/home/home';
import {About} from './components/about/about';
import {Entities} from './components/entities/entities';

@Component({
  selector: 'app'
})
@RouteConfig([
  { path: '/', component: Home, as: 'home' },
  { path: '/about', component: About, as: 'about' } ,
  { path: '/entities', component: Entities, as: 'entities' }
])
@View({
  templateUrl: './app.html?v=<%= VERSION %>',
  directives: [RouterOutlet, RouterLink]
})
class App {} 


bootstrap(App, [routerInjectables,HTTP_BINDINGS]);

'use strict';

var gulp = require('gulp');
var bump = require('gulp-bump');
var concat = require('gulp-concat');
var filter = require('gulp-filter');
var inject = require('gulp-inject');
var minifyCSS = require('gulp-minify-css');
var minifyHTML = require('gulp-minify-html');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var template = require('gulp-template');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');

var del = require('del');
var fs = require('fs');
var path = require('path');
var join = path.join;
var runSequence = require('run-sequence');
var semver = require('semver');
var series = require('stream-series');

var express = require('express');
var serveStatic = require('serve-static');
var openResource = require('open');

var tinylr = require('tiny-lr')();
var connectLivereload = require('connect-livereload');

// --------------
// Configuration.
var APP_BASE = '/';
var PATH = 'app';

var PORT = 8080;
var LIVE_RELOAD_PORT = 8081;


// --------------
// Serve dev.

gulp.task('serve', [ 'livereload'], function () {
  watch('./app/**', function (e) {
      notifyLiveReload(e);
  });
  serveSPA('dev');
});

// --------------
// Livereload.

gulp.task('livereload', function() {
  tinylr.listen(LIVE_RELOAD_PORT);
});

// --------------
// Utils.

function notifyLiveReload(e) {
  var fileName = e.path;
  tinylr.changed({
    body: {
      files: [fileName]
    }
  });
}


function serveSPA(env) {
  var app;
   app = express().use(APP_BASE, connectLivereload({ port: LIVE_RELOAD_PORT }), serveStatic(join(__dirname, PATH)));
  app.all(APP_BASE + '*', function (req, res, next) {
    res.sendFile(join(__dirname, PATH, 'index.html'));
  });
  console.log('===>',PORT,APP_BASE);
  app.listen(PORT, function () {
    openResource('http://0.0.0.0:' + PORT + APP_BASE);
  });
}
